tool
extends VisualScriptCustomNode

# The name of the custom node as it appears in the search.
func _get_caption():
	return "Delete On Contact"

func _get_category():
	return "UBODot"

# The text displayed after the input port / sequence arrow.
func _get_text():
	return ""

func _get_input_value_port_count():
	return 3

# The types of the inputs per index starting from 0.
func _get_input_value_port_type(idx):
	match idx:
		0:
			return TYPE_OBJECT
		1:
			return TYPE_STRING
		2:
			return TYPE_STRING


# The text displayed before each output node per index.
func _get_input_value_port_name(idx):
	match idx:
		0:
			return "Self"
		1:
			return "Groupe 2"
		2:
			return "Groupe 2"


func _has_input_sequence_port():
	return true

# The number of output sequence ports to use
# (has to be at least one if you have an input sequence port).
func _get_output_sequence_port_count():
	return 1

func _step(inputs, outputs, start_mode, working_mem):
	
	print(get_tree().get_current_scene())
	var zis = _get_base_node()
	print(zis)

	for bod in zis.get_colliding_bodies():
		if zis.is_in_group(inputs[1]) && bod.is_in_group(inputs[2]) || zis.is_in_group(inputs[2]) && bod.is_in_group(inputs[1]) :
			bod.queue_free()
			zis.queue_free()
	return 0

func _get_base_node():
	var base_path = self.resource_path
	var nullptr = 0
	var script = get_visual_script();

	var main_loop = Engine.get_main_loop()
	var scene_tree = main_loop as SceneTree

	if (!scene_tree):
		return nullptr

	var edited_scene = scene_tree.get_edited_scene_root()

	if (!edited_scene):
		return nullptr

	var script_node = _find_script_node(edited_scene, edited_scene, script);

	if (!script_node):
		return nullptr

	if (!script_node.has_node(base_path)):
		return nullptr

	var path_to = script_node.get_node(base_path)

	return path_to;

func _find_script_node(p_edited_scene, p_current_node, script):
	var nullptr = 0
	if (p_edited_scene != p_current_node && p_current_node.get_owner() != p_edited_scene):
		return nullptr

	var scr = p_current_node.get_script();

	if (scr.is_valid() && scr == script):
		return p_current_node

	for i in range(p_current_node.get_child_count()):
		var n = _find_script_node(p_edited_scene, p_current_node.get_child(i), script)
		if (n):
			return n

	return nullptr

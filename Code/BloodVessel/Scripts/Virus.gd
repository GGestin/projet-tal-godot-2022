extends "res://Scripts/Cell.gd"

export(bool) var stickWall = false

func _on_Virus_body_entered(body):
	var regex = RegEx.new()
	regex.compile("Anticorps[0-9]*")
	var resultA = regex.search(body.name)

	if(resultA) :
		var newCureCell = load("res://Assets/Bloodcell/BloodCell.tscn").instance()

		newCureCell.cure = true
		newCureCell.translate(translation)
		queue_free()
		get_parent().add_child(newCureCell)
		print("heroes never die")
	if(stickWall):
		regex.compile("Virus[0-9]*")
		var resultV = regex.search(body.name)
		if(body.name =="CurvedVein"):
			sleeping = true
		else :
			if (resultV && body.sleeping):
				sleeping = true

extends "res://Scripts/Cell.gd"
export(int) var probability_virus = 1
var cure = false
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if(cure):

		var material = $MeshInstance.get_surface_material(0).duplicate()
		material.albedo_color = Color(0, 0, 1)
		$MeshInstance.set_surface_material(0, material)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BloodCell1_body_entered(body):
	if(!cure):
		var regex = RegEx.new()
		regex.compile("Virus[0-9]*")
		var result = regex.search(body.name)
		if(result) :
			var random_number = randi()% probability_virus
			if(random_number == 0):
				print("infected")
				var newInfectedCell = load("res://Assets/Virus/Virus.tscn").instance()
				newInfectedCell.translate(translation)
				queue_free()
				get_parent().add_child(newInfectedCell)
				

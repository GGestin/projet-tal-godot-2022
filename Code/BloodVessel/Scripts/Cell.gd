extends RigidBody

export(float,10,20) var speed
var reset = false
var init_x
var init_y
var init_z

func _ready():
	translate(Vector3(0,0,0))
	add_central_force(Vector3.ZERO.normalized()*speed)

func _integrate_forces(state):
	if(reset):
		var coord = state.get_transform()
		coord.origin.x = init_x
		state.set_transform(coord)
		add_central_force(Vector3.ZERO.normalized()*speed)
		reset = false
	else :
		randomize()
		var x = rand_range(0,75)
		var y = rand_range(-75,75)
		var z = rand_range(-75,75)
		var force = Vector3(x,y,z)
		add_central_force(force.normalized()*speed)
		

		

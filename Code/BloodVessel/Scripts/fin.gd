extends Area
export (NodePath) var other_portal
onready var node = get_node(other_portal)



func _on_bluePortal_body_entered(body):
	
	var regex = RegEx.new()
	regex.compile("BloodCell[0-9]*")
	var resultC = regex.search(body.name)
	regex.compile("Virus[0-9]*")
	var resultV = regex.search(body.name)
	regex.compile("Anticorps[0-9]*")
	var resultA = regex.search(body.name)
	if(resultC || resultV || resultA) :
		body.reset = true
		body.init_x = node.translation[0]+1

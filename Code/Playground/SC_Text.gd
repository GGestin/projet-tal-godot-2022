@tool
extends VisualScriptCustomNode

func _get_caption():
	return "UI Text Element"

func _get_category():
	return "SimCell UI"

func _get_text():
	return ""

func _get_input_value_port_count():
	return 1

# The types of the inputs per index starting from 0.
func _get_input_value_port_type(idx):
	return TYPE_STRING

# The text displayed before each output node per index.
func _get_input_value_port_name(idx):
	return "Text to show"

func _has_input_sequence_port():
	return true

func _get_output_value_port_count():
	return 0

# The types of outputs per index starting from 0.
func _get_output_value_port_type(idx):
	return TYPE_OBJECT

func _get_output_sequence_port_count():
	return 1

func _step(inputs, outputs, start_mode, working_mem):
	if start_mode ==  START_MODE_BEGIN_SEQUENCE:
		var vat = get()

	var x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	var y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))

	# The outputs array is used to set the data of the output ports.

	outputs[0] = Vector2(x, y)

	# Return the error string if an error occurred, else the id of the next sequence port.
	return 0

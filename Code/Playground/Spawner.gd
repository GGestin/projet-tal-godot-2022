@tool
extends Node3D

@onready var Point : Position3D = get_children()[0]
@onready var Zone : MeshInstance3D = get_children()[1]
@onready var _timer = Timer.new()

@export var active : bool = true
@export var spawner_zone : bool = false :
	get:
		return spawner_zone
	set(value):
		if Zone != null && Point != null:
			Zone.set_visible(value)
			Point.set_visible(!value)
		spawner_zone = value

@export var source_object : PackedScene
@export var spawner_size : Vector3 = Vector3.ONE :
	get:
		return spawner_size
	set(value):
		Zone.scale = value
		spawner_size = value

@export var object_to_spawn : int = 1
@export var seconds : float = 1
@export var random_rotation : bool = true
@export var random_scale : bool = false
@export var scale_range : Vector2 = Vector2.ONE

func _ready():
	if Engine.is_editor_hint():
		Point = get_children()[0]
		Zone = get_children()[1]
		
	else:
		Point.set_visible(false)
		Zone.set_visible(false)

		add_child(_timer)
		_timer.connect("timeout", spawn)
		_timer.set_wait_time(seconds)
		_timer.set_one_shot(false) # Make sure it loops
		_timer.start()

func spawn():
	for i in range(object_to_spawn):
		var node = source_object.instantiate()
		if random_rotation:
			node.set_rotation(Vector3(randf(), randf(), randf()))
		if random_scale:
			for ()
			node.get_children().set_scale(Vector3.ONE * randf_range(scale_range.x, scale_range.y))
		if spawner_zone:
			node.set_translation(Vector3(randf_range(-spawner_size.x/2, spawner_size.x/2), randf_range(-spawner_size.y/2, spawner_size.y/2), randf_range(-spawner_size.z/2, spawner_size.z/2)))
		add_child(node)

func _process(delta):
	if not Engine.is_editor_hint():
		if not active && not _timer.is_stopped():
			_timer.stop()
		if active && _timer.is_stopped():
			_timer.start()

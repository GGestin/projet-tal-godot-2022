@tool 
extends Node3D

@export var ZoneSpawn : bool = true :
	get:
		return ZoneSpawn
	set(value):
		set_type(value)

@export var ZoneScale : Vector3 = Vector3.ONE :
	get:
		return ZoneScale
	set(value):
		zone_scale(value)

@export var ObjectToClone : Resource
@export var numberToSpawn : int = 1
@export var randomRotation : bool = false
@export var randomScale : bool = false
@export var scale_range : Vector2 = Vector2(0.5, 1.5)
@export var frequency : int = 60
@export var on_shortcut : bool = false
@export var shortcut : InputEventKey 

var cpt : int = -1
var Point
var Zone

func _ready():
	Point = get_child(0)
	Zone = get_child(1)
	if not Engine.is_editor_hint() :
		Zone.hide()
		Point.hide()

func _process(delta):
	if not Engine.is_editor_hint() :
		if frequency > 0:
			if cpt%frequency == 0:
				spawn()
		else:
			if cpt == -1:
				spawn()
		cpt += 1

func _unhandled_key_input(event):
	if (event.scancode == shortcut.scancode && on_shortcut):
		spawn()

func spawn():
	for i in range(numberToSpawn):
		var node = ObjectToClone.instance()
		if randomRotation:
			node.set_rotation(Vector3(randf(), randf(), randf()))
		if randomScale:
			node.set_scale(Vector3.ONE * randf_range(scale_range.x, scale_range.y))
		if ZoneSpawn:
			node.set_translation(Vector3(randf_range(-ZoneScale.x/2, ZoneScale.x/2), randf_range(-ZoneScale.y/2, ZoneScale.y/2), randf_range(-ZoneScale.z/2, ZoneScale.z/2)))
		add_child(node)

func set_type(truc):
	Point = get_child(0)
	Zone = get_child(1)
	if truc:
		Point.hide()
		Zone.show()
		ZoneSpawn = true
	else:
		Point.show()
		Zone.hide()
		ZoneSpawn = false

func zone_scale(news):
	Zone = get_child(1)
	Zone.scale = news
	ZoneScale = news
